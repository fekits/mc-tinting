require('../css/demo2.scss');
// import '../../lib/theme/mc-tinting@theme=default.scss';
// import '../../lib/theme/mc-tinting@theme=chrome.scss';
// import '../../lib/theme/mc-tinting@theme=github.scss';

// import './route';

// 代码着色插件
let McTinting = require('../../npm/mc-tinting.umd.min');

// 新建立一个默认的代码着色实例
new McTinting({
  el: '.mc-tinting',
  theme: 'vscode',
  number: 1
});

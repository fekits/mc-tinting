// import '../css/dark.scss';
require('../css/dark.scss');

// 代码着色插件
// let McTinting = require('../../npm/mc-tinting.umd.min');
let McTinting = require('../../npm/mc-tinting.umd.min');

// 新建立一个默认的代码着色实例
new McTinting({
  el: '.mc-tinting',
  theme: 'dark',
  number: 1
});

require('../css/main.scss');
require('../../lib/theme/mc-tinting@theme=vscode.scss');
// import '../../lib/theme/mc-tinting@theme=chrome.scss';
// import '../../lib/theme/mc-tinting@theme=github.scss';

// import './route';

// 代码着色插件
let McTinting = require('../../npm/mc-tinting.umd.min');

// 新建立一个默认的代码着色实例
let a = new McTinting({
  el: '.mc-tinting',
  theme: 'vscode',
  number: true
});

setTimeout(() => {
  let aaa = `<pre class="mc-tinting" lang="html" type="HTML">
  &lt;!DOCTYPE html&gt;
  &lt;html&gt;
  &lt;head&gt;
    &lt;!--直接引用代码着色插件--&gt;
    &lt;script src="https://fekit.asnowsoft.com/plugins/mc-tinting/js/mc-tinting.umd.min.js"&gt;&lt;/script&gt;
    &lt;link href="https://fekit.asnowsoft.com/plugins/mc-tinting/theme/mc-tinting@theme=default.css" rel="stylesheet"&gt;
    &lt;script&gt;
      // 新建一个实例，class为"mc-tinting"的pre标签的代码着色，设置主题为default
      new window.McTinting({
        el:'.mc-tinting',
        theme: 'default',
        number: true        // 显示行号
      });
    &lt;/script&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;!--注意HTML标签上一定要写语言哦--&gt;
    &lt;pre class="mc-tinting" lang="javascript"&gt;
      ...
    &lt;/pre&gt;
  
    &lt;h5 p-for="$menu.list as $item" class="{$item.active?'on':''}"&gt;
    &lt;/h5&gt;
  &lt;/body&gt;
  &lt;/html&gt;
    </pre>`;

  document.getElementById('aaa').innerHTML = aaa;
  a.refresh();
}, 3000);

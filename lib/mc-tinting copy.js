/**
 * <pre>标签javascript语法高亮显示插件，目的是对写在<pre>标签的内的源代码示例自动加上语法高亮。
 *
 * @param {Object} param                   指定需要语法高亮的DOM节点
 * @param {Object} param.el                指定需要语法高亮的DOM节点
 * @param {String} param.theme             指定主题
 *
 * 兼容: IE8+
 *
 * 作者: xiaojunbo
 * */

import preHtml from './languages/mc-tinting@lang=html';
import preCSS from './languages/mc-tinting@lang=css';
import preJS from './languages/mc-tinting@lang=javascript';

let McTinting = function (param = { el: null, theme: null }) {
  this.el = param.el || 'pre';
  this.theme = param.theme || 'default';
  this.number = param.number;
  this.init();
};

McTinting.prototype.init = function () {
  this.aPreDom = document.querySelectorAll(this.el) || [];
  this.aPreDom = [].slice.apply(this.aPreDom).filter((item) => {
    // console.log(item.getAttribute('tinted') === null);
    return item.getAttribute('tinted') === null;
  });
  let preNum = this.aPreDom.length;
  for (let i = 0; i < preNum; i++) {
    // 处理语法高亮
    (() => {
      // 获取单个标签
      let theDom = this.aPreDom[i];

      // 加入主题标识
      theDom.setAttribute('theme', this.theme);
      theDom.setAttribute('tinted', 'true');

      // 查询语言标识
      let theLang = theDom.getAttribute('language');
      // 创建一个副本
      let oldCode = theDom.innerHTML;
      let newCode = theDom.innerHTML;
      newCode = newCode.trim();
      // newCode = newCode.replace(/ *$/, '');

      // console.log(newCode);

      // 将&字符码转换成&
      newCode = newCode.replace(/&amp;/g, '&');

      // HTML语法处理
      if (theLang === 'html') {
        // 括号处理
        newCode = newCode.replace(/</g, '&lt;').replace(/>/g, '&gt;');

        // 内联CSS
        let cssCode = [];
        newCode = newCode.replace(/(&lt;style\s*&gt;)([\s\S]*?)(&lt;\/style&gt;)/g, (g0, g1, g2, g3) => {
          cssCode.push(preCSS(g2));
          return `${g1}{__CSS_TINTING${cssCode.length}__}${g3}`;
        });

        // 内联JS
        let jsCode = [];
        newCode = newCode.replace(/(&lt;script\s*&gt;)([\s\S]*?)(&lt;\/script&gt;)/g, (g0, g1, g2, g3) => {
          jsCode.push(preJS(g2));
          return `${g1}{__JS_TINTING${jsCode.length}__}${g3}`;
        });

        newCode = preHtml(newCode);

        // 内联JS还原
        jsCode.forEach((item, index) => {
          let reg = new RegExp(`{__JS_TINTING${index + 1}__}`, 'g');
          newCode = newCode.replace(reg, item);
        });

        // 内联CSS还原
        cssCode.forEach((item, index) => {
          let reg = new RegExp(`{__CSS_TINTING${index + 1}__}`, 'g');
          newCode = newCode.replace(reg, item);
        });
      }

      // CSS语法处理
      if (theLang === 'css') {
        newCode = preCSS(newCode);
      }

      // JS语法处理
      if (theLang === 'javascript') {
        newCode = preJS(newCode);
      }

      let numCode = '';
      let digit = 2;
      if (this.number) {
        let num = 0;
        let _oldCode = oldCode.trim();
        numCode = _oldCode.replace(/([^\n]*\n|[^\n]+$)/g, (res) => {
          console.log(res);
          num += 1;
          return '<code>' + num + '</code>';
        });
        numCode = numCode.trim();
        numCode = `<code class="mc-tinting-number">${numCode}</code>`;
        digit = ('' + num).length;
      }

      // 是否为文件
      let isFile = this.aPreDom[i].getAttribute('file');
      let headCode = isFile ? '<code class="mc-tinting-head"></code>' : '';
      let footCode = isFile ? '<code class="mc-tinting-foot"></code>' : '';

      // 替换内容
      theDom.innerHTML = `${headCode}<code class="mc-tinting-area" data-number="${this.number ? 1 : 0}" data-digit="${digit}">${numCode}<code class="mc-tinting-body">${newCode}</code></code>${footCode}`;
    })();
  }
};

McTinting.prototype.refresh = function () {
  this.init();
};

module.exports = McTinting;

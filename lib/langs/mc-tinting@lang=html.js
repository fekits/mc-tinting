module.exports = (code = '') => {
  // 注释处理
  let comCode = [];
  code = code.replace(/&lt;!--(.|\n)*?--&gt;/g, (g1) => {
    comCode.push(`<code class="h-com">${g1}</code>`);
    return `{__COM_TINTING${comCode.length}__}`;
  });

  // 属性处理
  let keyCode = [];
  // console.log(code);
  code = code.replace(/(\s[\w-]*)(=)((["'])([^\4]*?)(\4))/g, (g0, g1, g2, g3, g4, g5, g6) => {
    // console.log(g3);
    keyCode.push(`<code class="h-key">${g1}</code><code class="h-equ">${g2}</code><code class="h-equ">${g4}</code><code class="h-str">${g5}</code><code class="h-equ">${g6}</code>`);
    return `{__KEY_TINTING${keyCode.length}__}`;
  });

  let docCode = [];
  code = code.replace(/(&lt;)(!DOCTYPE)\s+(html)(\/?&gt;)/g, (g0, g1, g2, g3, g4) => {
    docCode.push(`<code class="h-doc"><code class="h-tag"><code class="h-bra">${g1}</code>${g2}</code> <code class="h-key">${g3}</code><code class="h-tag"><code class="h-bra">${g4}</code></code></code>`);
    return `{__DOC_TINTING${docCode.length}__}`;
  });

  // 标签处理
  let tagCode = [];
  code = code.replace(/(&lt;)(\/?[a-zA-Z0-6]+)([\s\S]*?)(\/?&gt;)/g, (g0, g1, g2, g3, g4) => {
    tagCode.push(`<code class="h-tag"><code class="h-bra">${g1}</code>${g2}</code>${g3}<code class="h-tag"><code class="h-bra">${g4}</code></code>`);
    return `{__TAG_TINTING${tagCode.length}__}`;
  });

  // 标签还原
  // tagCode.forEach((item, index) => {
  //   let reg = new RegExp(`{__TAG_TINTING${index + 1}__}`, 'g');
  //   code = code.replace(reg, item);
  // });
  for (let i = 0, len = tagCode.length; i < len; i++) {
    let item = tagCode[i];
    let reg = new RegExp(`{__TAG_TINTING${i + 1}__}`, 'g');
    code = code.replace(reg, item);
  }

  // 属性还原
  // keyCode.forEach((item, index) => {
  //   let reg = new RegExp(`{__KEY_TINTING${index + 1}__}`, 'g');
  //   code = code.replace(reg, item);
  // });
  for (let i = 0, len = keyCode.length; i < len; i++) {
    let item = keyCode[i];
    let reg = new RegExp(`{__KEY_TINTING${i + 1}__}`, 'g');
    code = code.replace(reg, item);
  }

  // 注释还原
  // comCode.forEach((item, index) => {
  //   let reg = new RegExp(`{__COM_TINTING${index + 1}__}`, 'g');
  //   code = code.replace(reg, item);
  // });
  for (let i = 0, len = comCode.length; i < len; i++) {
    let item = comCode[i];
    let reg = new RegExp(`{__COM_TINTING${i + 1}__}`, 'g');
    code = code.replace(reg, item);
  }

  // 注释还原
  // docCode.forEach((item, index) => {
  //   let reg = new RegExp(`{__DOC_TINTING${index + 1}__}`, 'g');
  //   code = code.replace(reg, item);
  // });
  for (let i = 0, len = docCode.length; i < len; i++) {
    let item = docCode[i];
    let reg = new RegExp(`{__DOC_TINTING${i + 1}__}`, 'g');
    code = code.replace(reg, item);
  }

  return '<code data-lang="html">' + code + '</code>';
};

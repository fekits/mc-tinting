module.exports= (code = '') => {
  // 注释
  let comData = [];
  // 单行注释
  code = code.replace(/(^|[^\b(http:|https:|"|')])(\/\/[^:][^\n]*)/g, (S0, S1, S2) => {
    comData.push(`<code class="c-err">${S2}</code>`);
    return `${S1}{__COM_TINTING${comData.length}__}`;
  });

  // 多行注释
  code = code.replace(/(\/\*[^*][\s\S]*?\*\/)/g, (S1) => {
    comData.push(`<code class="c-com">${S1}</code>`);
    return `{__COM_TINTING${comData.length}__}`;
  });

  // 对象
  let objData = [];
  code = code.replace(/({\s?[^{}]*\s})/g, (S0, S1) => {
    // console.log(S1);
    S1 = S1.replace(/([\w-]+)(\s?:\s?)([^;\n}]*)([;}]?)/g, (S0, S1, S2, S3, S4) => {
      objData.push(`<code class="c-key">${S1}</code><code class="c-sym">${S2}</code><code class="c-val">${S3}</code><code class="c-sym">${S4}</code>`);
      return `{__OBJ_TINTING${objData.length}__}`;
    });
    return S1;
  });

  // 标签
  let tagData = [];
  code = code.replace(/([:\w-*#.@&]+)(\s?,|\s?{|\s?\s|\s?>)/g, (S0, S1, S2) => {
    // console.log(S1);
    if (/^\w/.test(S1)) {
      tagData.push(`<code class="c-tag">${S1}</code>`);
    } else {
      tagData.push(`<code class="c-sel">${S1}</code>`);
    }
    return `{__TAG_TINTING${tagData.length}__}${S2}`;
  });

  // 标签还原
  // tagData.forEach((item, index) => {
  //   let reg = new RegExp(`{__TAG_TINTING${index + 1}__}`, 'g');
  //   code = code.replace(reg, item);
  // });
  for (let i = 0, len = tagData.length; i < len; i++) {
    let item = tagData[i];
    let reg = new RegExp(`{__TAG_TINTING${i + 1}__}`, 'g');
    code = code.replace(reg, item);
  }

  // 对象还原
  // objData.forEach((item, index) => {
  //   let reg = new RegExp(`{__OBJ_TINTING${index + 1}__}`, 'g');
  //   code = code.replace(reg, item);
  // });
  for (let i = 0, len = objData.length; i < len; i++) {
    let item = objData[i];
    let reg = new RegExp(`{__OBJ_TINTING${i + 1}__}`, 'g');
    code = code.replace(reg, item);
  }

  // 注释还原
  // comData.forEach((item, index) => {
  //   let reg = new RegExp(`{__COM_TINTING${index + 1}__}`, 'g');
  //   code = code.replace(reg, item);
  // });
  for (let i = 0, len = comData.length; i < len; i++) {
    let item = comData[i];
    let reg = new RegExp(`{__COM_TINTING${i + 1}__}`, 'g');
    code = code.replace(reg, item);
  }

  return '<code data-lang="css">' + code + '</code>';
};

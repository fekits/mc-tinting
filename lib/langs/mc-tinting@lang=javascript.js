let preHtml = require('./mc-tinting@lang=html');

module.exports = (code) => {
  // 正则内部处理
  function regCode(S3) {
    let S3Data = [];

    // 元字符
    S3 = S3.replace(/(\\(w|W|d|D|s|S|b|B|0|n|f|r|t|v|xxx|xdd|uxxxx))/g, (S1) => {
      // console.log(S1);
      S3Data.push(`<code class="j-met">${S1}</code>`);
      return `{__REG_S3DATA${S3Data.length}__}`;
    });

    // 转义符
    S3 = S3.replace(/\\(\\|\^|\.|\/|\||\*|\(|\)|\[|]|\+|\?|\$)/g, (S1) => {
      // console.log(S1);
      S3Data.push(`<code class="j-esc">${S1}</code>`);
      return `{__REG_S3DATA${S3Data.length}__}`;
    });

    // 功能符
    S3 = S3.replace(/(\^|\|)/g, (s1) => {
      S3Data.push(`<code class="j-spe">${s1}</code>`);
      return `{__REG_S3DATA${S3Data.length}__}`;
    });

    // 括号
    S3 = S3.replace(/(\(|\)|\[|])/g, (s1) => {
      S3Data.push(`<code class="j-bra">${s1}</code>`);
      return `{__REG_S3DATA${S3Data.length}__}`;
    });

    // 运算符处理
    S3 = S3.replace(/(\+|\*|\?)/g, (s1) => {
      S3Data.push(`<code class="j-ope">${s1}</code>`);
      return `{__REG_S3DATA${S3Data.length}__}`;
    });

    // 回收
    // S3Data.forEach((item, index) => {
    //   let reg = new RegExp(`{__REG_S3DATA${index + 1}__}`, 'g');
    //   S3 = S3.replace(reg, item);
    // });
    for (let i = 0, len = S3Data.length; i < len; i++) {
      let item = S3Data[i];
      let reg = new RegExp(`{__REG_S3DATA${i + 1}__}`, 'g');
      S3 = S3.replace(reg, item);
    }
    return S3;
  }

  // 注释处理
  (() => {
    let tasks = [];

    // 尖括号
    code = code.replace(/</g, '&lt;').replace(/>/g, '&gt;');

    // 注释
    let comData = [];
    // 单行注释
    code = code.replace(/((^|[^(http:|https:|"|')])\/\/[^:][^\n]*)/g, ($1) => {
      comData.push(`<code class="j-com">${$1}</code>`);
      return `{__COM_TINTING${comData.length}__}`;
    });
    // 多行注释(单星号)
    code = code.replace(/(\/\*[^*][\s\S]*?\*\/)/g, (g1) => {
      comData.push(`<code class="j-com">${g1}</code>`);
      return `{__COM_TINTING${comData.length}__}`;
    });
    // 多行注释(双星号)
    code = code.replace(/(\/\*\*[\s\S]*?\*\/)/g, ($1) => {
      $1 = $1.replace(/(@param)(\s+)/g, '<code class="j-com">$1</code>$2');
      comData.push(`<code class="j-com">${$1}</code>`);
      return `{__COM_TINTING${comData.length}__}`;
    });
    tasks.unshift(comData);

    // 正则
    let regData = [];
    code = code.replace(/(\.replace\()(\/)([^\n]*)(\/)([gimsuy]?[gimsuy]?[gimsuy]?[gimsuy]?[gimsuy]?[gimsuy]?,)/g, (S0, S1, S2, S3, S4, S5) => {
      // 正则内部处理
      S3 = regCode(S3);
      regData.push(`<code class="j-reg-area"><code class="j-reg">${S2}</code><code class="j-str">${S3}</code><code class="j-reg">${S4}</code></code><code class="j-str">${S5}</code>`);
      return `${S1}{__REG_TINTING${regData.length}__}`;
    });

    // 正则-字符串
    code = code.replace(/(\/)([^\n]*)(\/)(\.test\()/g, (S0, S1, S2, S3, S4) => {
      // 正则内部处理
      S2 = regCode(S2);
      regData.push(`<code class="j-reg">${S1}<code class="j-str">${S2}</code>${S3}</code>`);
      return `{__REG_TINTING${regData.length}__}${S4}`;
    });

    // 正则-RegExp
    code = code.replace(/(new\sRegExp\()('|"|`)([^\n]*)(\2)(\s?,\s?)('|"|`)(g?|i?|m?|s?|u?|y?)(\6)/g, (S0, S1, S2, S3, S4, S5, S6, S7, S8) => {
      // 正则内部处理
      S2 = regCode(S2);
      regData.push(`<code class="j-reg"><code class="j-str">${S2}</code><code class="j-str">${S3}</code><code class="j-sym">${S4}</code><code class="j-str">${S5}${S6}${S7}${S8}</code></code>`);
      return `${S1}{__REG_TINTING${regData.length}__}`;
    });
    tasks.unshift(regData);

    // 模版字符串
    let strData = [];
    code = code.replace(/(`)[^\n(\1)]*?\1/g, (A1) => {
      // 模板字符串中的HTML标签
      A1 = preHtml(A1);

      // 模板字符串中的变量 -> ${xxx}
      A1 = A1.replace(/(\${)([^{}]*)(})/g, (B0, B1, B2, B3) => {
        // 模板字符串中的对象属性-> ${obj.xxx}
        B2 = B2.replace(/(\.)(\w*)(\b)/g, (C0, C1, C2, C3) => {
          return `${C1}<code class="j-sub">${C2}</code>${C3}`;
        });
        return `<code class="j-var"><code class="j-tpl-var">${B1}</code>${B2}<code class="j-tpl-var">${B3}</code></code>`;
      });

      strData.push(`<code class="j-tpl">${A1}</code>`);
      return `{__STR_TINTING${strData.length}__}`;
    });

    // 字符串
    code = code.replace(/('|")[^\n(\1)]*?\1/g, ($1) => {
      strData.push(`<code class="j-str">${$1}</code>`);
      return `{__STR_TINTING${strData.length}__}`;
    });

    tasks.unshift(strData);

    // 函数名称
    let funData = [];

    /*
      1、处理以下几种类型:
          xxx.funName()
          xxx.funName ()
    */
    code = code.replace(/(\.)(\w+)(\s?\()/g, (S0, S1, S2, S3) => {
      funData.push(`<code class="j-fun">${S2}</code>`);
      return `${S1}{__FUN_TINTING${funData.length}__}${S3}`;
    });

    /*
      2、处理以下几种类型:
          funName=()
          funName = ()
    */
    code = code.replace(/([^\\/]\b)(\w+)(\s?=\s?\()/g, (S0, S1, S2, S3) => {
      funData.push(`<code class="j-fun">${S2}</code>`);
      return `${S1}{__FUN_TINTING${funData.length}__}${S3}`;
    });

    /*
      3、处理以下几种类型:
            =funName()
            =funName ()
            = funName()
            = funName ()
    */
    code = code.replace(/(=\s?)((?!function)\w+)(\s?\()/g, (S0, S1, S2, S3) => {
      funData.push(`<code class="j-fun">${S2}</code>`);
      return `${S1}{__FUN_TINTING${funData.length}__}${S3}`;
    });

    /*
      4、处理以下几种类型:
          {
            funName:()=>{}
            funName: ()=>{}
            funName :()=>{}
            funName : ()=>{}
            funName:function(){}
            funName: function(){}
            funName : function(){}
            funName : function (){}
          }
    */
    code = code.replace(/([^\\/]\b)(\w+)(\s?:\s?(function)?\s?\()/g, (S0, S1, S2, S3) => {
      funData.push(`<code class="j-fun">${S2}</code>`);
      return `${S1}{__FUN_TINTING${funData.length}__}${S3}`;
    });

    /*
      5、处理以下几种类型:
            funName(){}
    */
    code = code.replace(/([^\\/]\b)((?!if|for|switch|while|function)\w+)(\s?\()/g, (S0, S1, S2, S3) => {
      funData.push(`<code class="j-fun">${S2}</code>`);
      return `${S1}{__FUN_TINTING${funData.length}__}${S3}`;
    });
    // tasks.unshift(funData);

    // 变量名称
    let varData = [];
    code = code.replace(/(\b|\s)(let\s+|var\s+|import\s+)(\w+)(\s*(=|from)\s*)/gm, (S0, S1, S2, S3, S4) => {
      varData.push(`<code class="j-var">${S3}</code>`);
      return `${S1}${S2}{__VAR_TINTING${varData.length}__}${S4}`;
    });
    // tasks.unshift(varData);

    // 关键词
    let retData = [];
    code = code.replace(/(\s|\b)(let|var|const|import|from|true|false|class|new|this|export\sdefault|function|return|for|if|else|else\sif)(\s+|\b)/g, (S0, S1, S2, S3) => {
      retData.push(`<code class="j-ret">${S2}</code>`);
      return `${S1}{__RET_TINTING${retData.length}__}${S3}`;
    });
    // tasks.unshift(retData);

    // 对象属性
    let keyData = [];
    code = code.replace(/([^=][,{\s\n]+)([^,\n\s.({})':=]*)(\s*:)/g, (S0, S1, S2, S3) => {
      keyData.push(`<code class="j-key">${S2}</code>`);
      return `${S1}{__KEY_TINTING${keyData.length}__}${S3}`;
    });
    // tasks.unshift({ type: 'SUB', code: objData });

    // 对象子集
    let subData = [];
    code = code.replace(/(\.)(\w*)(\b)/g, (S0, S1, S2, S3) => {
      subData.push(`<code class="j-sub">${S2}</code>`);
      return `${S1}{__SUB_TINTING${subData.length}__}${S3}`;
    });
    // tasks.unshift({ type: 'SUB', code: subData });

    // 数字
    let numData = [];
    code = code.replace(/(\s|\+\s?|\+=\s?|-\s?|-=\s?|\*\s?|\*=\s?|\/\s?|\/=\s?|=\s?|=\+\s?|=-\s?|=\*\s?|=\/\s?|:\s?|,\s?|\[\s?)(-?\d+\.?\d*)(\b)/g, (S0, S1, S2, S3) => {
      numData.push(`<code class="j-num">${S2}</code>`);
      return `${S1}{__NUM_TINTING${numData.length}__}${S3}`;
    });
    // tasks.unshift({ type: 'NUM', code: numData });

    // 对象
    let objData = [];
    code = code.replace(/(\b[a-zA-Z0-9_]*)(\.)/g, (S0, S1, S2) => {
      objData.push(`<code class="j-obj">${S1}</code>`);
      return `{__OBJ_TINTING${objData.length}__}${S2}`;
    });

    // 比较符
    let cooData = [];
    code = code.replace(/(===|!==|==|&lt;=|&gt;=|=&gt;|!=|&lt;|&gt;|\+\+|\|\||=|\?|:|;|,)/g, (S1) => {
      cooData.push(`<code class="j-coo">${S1}</code>`);
      return `{__COO_TINTING${cooData.length}__}`;
    });

    // console.log('---------------');
    // console.log(code);
    // console.log('---------------');

    // 符号
    // let symData = [];
    // code = code.replace(/(,|;)/gm, (s1) => {
    //   symData.push(`<code class="j-sym">${s1}</code>`);
    //   return `{__SYM_TINTING${symData.length}__}`;
    // });

    // 运算符处理
    // let opeData = [];
    // code = code.replace(/(\+|\*)/g, (s1) => {
    //   opeData.push(`<code class="j-ope">${s1}</code>`);
    //   return `{__OPE_TINTING${opeData.length}__}`;
    // });

    // 点，冒号，括号
    // let dotData = [];
    // code = code.replace(/(\.|:|\(|\)|\[|\]|\?)/g, (s1) => {
    //   dotData.push(`<code class="j-dot">${s1}</code>`);
    //   return `{__DOT_TINTING${dotData.length}__}`;
    // });

    // 等于号
    // let equData = [];
    // code = code.replace(/(=)/g, (s1) => {
    //   equData.push(`<code class="j-equ">${s1}</code>`);
    //   return `{__EQU_TINTING${equData.length}__}`;
    // });

    //-------------------

    // 等于号还原
    // equData.forEach((item, index) => {
    //   let reg = new RegExp(`{__EQU_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });

    // 点，冒号，括号
    // dotData.forEach((item, index) => {
    //   let reg = new RegExp(`{__DOT_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });

    // 运算符处理
    // opeData.forEach((item, index) => {
    //   let reg = new RegExp(`{__OPE_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });

    // 符号
    // symData.forEach((item, index) => {
    //   let reg = new RegExp(`{__SYM_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });

    // 比较符还原
    // cooData.forEach((item, index) => {
    //   let reg = new RegExp(`{__COO_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = cooData.length; i < len; i++) {
      let item = cooData[i];
      let reg = new RegExp(`{__COO_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 对象还原
    // objData.forEach((item, index) => {
    //   let reg = new RegExp(`{__OBJ_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = objData.length; i < len; i++) {
      let item = objData[i];
      let reg = new RegExp(`{__OBJ_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 数字还原
    // numData.forEach((item, index) => {
    //   let reg = new RegExp(`{__NUM_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = numData.length; i < len; i++) {
      let item = numData[i];
      let reg = new RegExp(`{__NUM_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 子集还原
    // subData.forEach((item, index) => {
    //   let reg = new RegExp(`{__SUB_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = subData.length; i < len; i++) {
      let item = subData[i];
      let reg = new RegExp(`{__SUB_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 对象还原
    // keyData.forEach((item, index) => {
    //   let reg = new RegExp(`{__KEY_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = keyData.length; i < len; i++) {
      let item = keyData[i];
      let reg = new RegExp(`{__KEY_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 关键词还原
    // retData.forEach((item, index) => {
    //   let reg = new RegExp(`{__RET_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = retData.length; i < len; i++) {
      let item = retData[i];
      let reg = new RegExp(`{__RET_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 变量还原
    // varData.forEach((item, index) => {
    //   let reg = new RegExp(`{__VAR_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = varData.length; i < len; i++) {
      let item = varData[i];
      let reg = new RegExp(`{__VAR_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 函数还原
    // funData.forEach((item, index) => {
    //   let reg = new RegExp(`{__FUN_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = funData.length; i < len; i++) {
      let item = funData[i];
      let reg = new RegExp(`{__FUN_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 字符还原
    // strData.forEach((item, index) => {
    //   let reg = new RegExp(`{__STR_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = strData.length; i < len; i++) {
      let item = strData[i];
      let reg = new RegExp(`{__STR_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 正则还原
    // regData.forEach((item, index) => {
    //   let reg = new RegExp(`{__REG_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = regData.length; i < len; i++) {
      let item = regData[i];
      let reg = new RegExp(`{__REG_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }

    // 注释还原
    // comData.forEach((item, index) => {
    //   let reg = new RegExp(`{__COM_TINTING${index + 1}__}`, 'g');
    //   code = code.replace(reg, item);
    // });
    for (let i = 0, len = comData.length; i < len; i++) {
      let item = comData[i];
      let reg = new RegExp(`{__COM_TINTING${i + 1}__}`, 'g');
      code = code.replace(reg, item);
    }
  })();
  return '<code data-lang="javascript">' + code + '</code>';
};

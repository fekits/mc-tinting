# @FEKIT/MC-TINTING

```$xslt
一款可扩展的代码高亮插件，目前仅扩展有前端代码语言（HTML+JS+CSS+SASS）；如需其它语言高亮可联系我。
```

### 索引

- [演示](#演示)
- [参数](#参数)
- [示例](#示例)
- [主题](#主题)
- [版本](#版本)
- [反馈](#反馈)

### 演示

HOME：[https://mcui.fekit.cn/#route=plugins/js/mc-tinting](https://mcui.fekit.cn/#route=plugins/js/mc-tinting)

DEMO：[https://fekit.cn/plugins/mc-tinting/](https://fekit.cn/plugins/mc-tinting/)

### 开始

下载项目:

NPM

```npm
npm i @fekit/mc-tinting
```

CDN

```html
<!-- 插件 -->
<script type="text/javascript" src="https://cdn.fekit.cn/@fekit/mc-tinting/mc-tinting.umd.min.js"></script>
<!-- 主题 -->
<link href="https://cdn.fekit.cn/@fekit/mc-tinting/theme/mc-tinting@theme=default.css" rel="stylesheet" />
```

### 参数

```$xslt
 {
   el          指定需要语法高亮的DOM节点
   theme       指定主题
   number      显示行号
 }
```

### 示例

JS

```javascript
// 代码着色插件
import McTinting from '@fekit/mc-tinting';

// 新建立一个默认的代码着色实例,所有pre标签代码高亮
new McTinting({
  theme: 'default'
});

// 新建立一个默认的代码着色实例,选择器".aaa"的元素代码高亮
new McTinting({
  el: '.aaa',
  theme: 'github'
});

// 显示行号
new McTinting({
  number: true
});
```

HTML

```html
<!DOCTYPE html>
<html lang="zh-CN" mcui="v1.0.1">
  <head>
    <meta charset="UTF-8" />
    <!-- 如果你没有用到NPM或SCSS的话可以直接引入CDN文件 -->
    <!-- 引入插件 -->
    <script type="text/javascript" src="https://cdn.fekit.cn/@fekit/mc-tinting/mc-tinting.umd.min.js"></script>
    <!-- 引入主题,这里演示引入默认主题。还有更多主题可以引用，也可以同时引用多款主题，详情请看【主题】 -->
    <link href="https://cdn.fekit.cn/@fekit/mc-tinting/theme/mc-tinting@theme=default.css" rel="stylesheet" />
    <title>MC-TINTING</title>
  </head>
  <body>
    <!--lang是指定代码语言可以是"javascript,html,css" file是添加头部和文件名 -->
    <pre lang="javascript" file="main.js">
new McTinting({
  el: '.aaa',
  theme: 'github'
});
    <pre>
  </body>
</html>


```

CSS/SCSS

```scss
// 引入主题，用到一个引一个，没用到可以不引。
@import '~@fekit/mc-tinting/theme/mc-tinting@theme=default.scss';
@import '~@fekit/mc-tinting/theme/mc-tinting@theme=chrome.scss';
@import '~@fekit/mc-tinting/theme/mc-tinting@theme=vscode.scss';
@import '~@fekit/mc-tinting/theme/mc-tinting@theme=dark.scss';
@import '~@fekit/mc-tinting/theme/mc-tinting@theme=github.scss';
@import '~@fekit/mc-tinting/theme/mc-tinting@theme=darcula.scss';
```

### 主题

- [theme/mc-tinting@theme=default.css](https://cdn.fekit.cn/@fekit/mc-tinting/theme/mc-tinting@theme=default.css)
- [theme/mc-tinting@theme=chrome.css](https://cdn.fekit.cn/@fekit/mc-tinting/theme/mc-tinting@theme=chrome.css)
- [theme/mc-tinting@theme=vscode.css](https://cdn.fekit.cn/@fekit/mc-tinting/theme/mc-tinting@theme=vscode.css)
- [theme/mc-tinting@theme=dark.css](https://cdn.fekit.cn/@fekit/mc-tinting/theme/mc-tinting@theme=dark.css)
- [theme/mc-tinting@theme=github.css](https://cdn.fekit.cn/@fekit/mc-tinting/theme/mc-tinting@theme=github.css)
- [theme/mc-tinting@theme=darcula.css](https://cdn.fekit.cn/@fekit/mc-tinting/theme/mc-tinting@theme=darcula.css)

### 版本
```$xslt
v1.2.7 [Latest version]
1. 修改由html标签设置data-number来控制显示行号。
```

```$xslt
v1.2.3
1. 添加了可设置显示行号的入参 number:true时显示行号
2. 在pre标答中添加了一个file="xxx.js"时可以将代码块显示为一个带有标题的头部使代码块看起来像是在一个编辑器中
```

```$xslt
v1.2.2
1. 优化一些主题的CSS样式
```

```$xslt
v1.2.1
1. 语言属性名language改成lang
```

```$xslt
v1.1.6
1. 修复一个底级错误，true关键词拼写错误
2. 修复主题行号比代码高度略偏下的问题
```

```$xslt
v1.1.5
1. 优化主题UI
```

```$xslt
v1.1.4
1. 优化主题UI
```

```$xslt
v1.1.3
1. 修复HTML标签带有数字的h1-h6数字未包含到标签名的BUG
2. 优化主题UI
```

```$xslt
v1.1.2
1. 新增一款主题"mc-tinting@theme=dark"
```

```$xslt
v1.1.2
1. 新增显示行号功能
```

```$xslt
v1.1.1
1. 新增一款主题chrome
2. 优化了一些主题样式
3. 细化了一些高亮类型
```

```$xslt
v1.1.0
1. 修复html语言<!DOCTYPE html>忘记高亮处理的BUG
2. 优化主题样式
```

```$xslt
v1.0.5
1. 非核心更新，编写文档
```

```$xslt
...
```

```$xslt
v1.0.1
1. 此版本精简了代码，取消了onshow和onhide，回调合并为一个then，可以通赤then回调的传参来判断是显示还是消失及其它一些操作。
```

#### 反馈

```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
